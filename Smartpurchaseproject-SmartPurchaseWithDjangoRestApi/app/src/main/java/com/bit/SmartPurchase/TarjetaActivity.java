package com.bit.SmartPurchase;

import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.bit.SmartPurchase.Controller.PagerController;
import com.bit.SmartPurchase.domain.ProductoDjango;
import com.bit.SmartPurchase.repositories.CompraRestRepository;
import com.google.android.material.tabs.TabItem;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;

public class TarjetaActivity extends AppCompatActivity {

    TabLayout tabLayout;
    ViewPager viewPager;
    TabItem tabDebito, tabCredito;
    PagerController pagerAdapter;
    Button btnTarjetaDebito, btnTarjetaCredito;
    TextInputEditText titularTarjeta, numeroTarjeta, codigoSeguridad, vencimiento;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tarjeta);
        InicializarVariables();

        pagerAdapter = new PagerController(getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(pagerAdapter);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                cargarFragment(tab, pagerAdapter);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                cargarFragment(tab, pagerAdapter);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                cargarFragment(tab, pagerAdapter);
            }
        });

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));


    }

    private void InicializarVariables(){
        tabLayout = findViewById(R.id.tabLayout);
        viewPager = findViewById(R.id.viewPager);
        tabDebito = findViewById(R.id.tabDebito);
        tabCredito = findViewById(R.id.tabCredito);
    }

    private void cargarFragment(TabLayout.Tab tab, PagerController pagerFragmentAdapter){
        if (tab.getPosition() == 0) {
            Toast.makeText(getApplicationContext(), "Tarjeta de debito", Toast.LENGTH_SHORT).show();
            pagerFragmentAdapter.notifyDataSetChanged();
            FrameLayout frameLayout = findViewById(R.id.fragment_debito);
            btnTarjetaDebito = frameLayout.findViewById(R.id.btnTarjDebito);
            btnTarjetaDebito.setOnClickListener(view-> {
                InicializarDebito();
                if(!validacionCamposVacios()) {
                    startActivity(new Intent(TarjetaActivity.this, agradecimientoActivity.class));
                }else
                {
                    Toast.makeText(TarjetaActivity.this, "No deben haber campos vacios", Toast.LENGTH_SHORT).show();
                }
            });
        }
        if (tab.getPosition() == 1) {
            Toast.makeText(getApplicationContext(), "Tarjeta de credito", Toast.LENGTH_SHORT).show();
            pagerFragmentAdapter.notifyDataSetChanged();
            FrameLayout frameLayout = findViewById(R.id.fragment_credito);
            btnTarjetaCredito = frameLayout.findViewById(R.id.btnTarjCredito);
            btnTarjetaCredito.setOnClickListener(view-> {
                InicializarCredito();
                if(!validacionCamposVacios()) {
                    startActivity(new Intent(TarjetaActivity.this, agradecimientoActivity.class));
                }else
                {
                    Toast.makeText(TarjetaActivity.this, "No deben haber campos vacios", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private void InicializarDebito(){
        FrameLayout frameLayout = findViewById(R.id.fragment_debito);
        titularTarjeta = frameLayout.findViewById(R.id.edTitTarjetaDebito);
        numeroTarjeta = frameLayout.findViewById(R.id.edNumTarjetaDebito);
        codigoSeguridad = frameLayout.findViewById(R.id.edCodTarjetaDebito);
        vencimiento = frameLayout.findViewById(R.id.edVencTarjetaDebito);
    }

    private void InicializarCredito(){
        FrameLayout frameLayout = findViewById(R.id.fragment_credito);
        titularTarjeta = frameLayout.findViewById(R.id.edTitTarjetaCredito);
        numeroTarjeta = frameLayout.findViewById(R.id.edNumTarjetaCredito);
        codigoSeguridad = frameLayout.findViewById(R.id.edCodTarjetaCredito);
        vencimiento = frameLayout.findViewById(R.id.edVencTarjetaCredito);
    }


    private boolean validacionCamposVacios(){
        return TextUtils.isEmpty(titularTarjeta.getText().toString()) ||
                TextUtils.isEmpty(numeroTarjeta.getText().toString()) ||
                        TextUtils.isEmpty(codigoSeguridad.getText().toString()) ||
                        TextUtils.isEmpty(vencimiento.getText().toString());

    }
}
