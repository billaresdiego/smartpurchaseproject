package com.bit.SmartPurchase.rest;

import com.bit.SmartPurchase.domain.ProductoDjango;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface RestApiInterface {

    @GET("/api/producto/")
    Call<List<ProductoDjango>> getProductosDjango();

    @GET("/api/producto/{id}")
    Call<ProductoDjango> getProductoDjango(@Path("id") int id);

    @FormUrlEncoded
    @POST("/api/producto/")
    Call<ProductoDjango> addProductoDjango(@Field("nombre") String nombre,
                                   @Field("precio") Integer precio,
                                   @Field("marca") String marca,
                                   @Field("imagen") String imagen);
}
