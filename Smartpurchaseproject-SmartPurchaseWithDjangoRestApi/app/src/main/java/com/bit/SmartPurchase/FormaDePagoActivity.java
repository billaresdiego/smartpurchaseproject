package com.bit.SmartPurchase;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.bit.SmartPurchase.domain.ProductoDjango;
import com.bit.SmartPurchase.repositories.CompraRestRepository;

import java.util.ArrayList;

import static com.bit.SmartPurchase.AgregarProductoActivity.*;

public class FormaDePagoActivity extends AppCompatActivity {

    CardView formaDePagoTarjeta, formaDePagoEfectivo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forma_pago);
        formaDePagoTarjeta = findViewById(R.id.card_view_tarjeta);
        formaDePagoEfectivo = findViewById(R.id.card_view_dinero);

        formaDePagoTarjeta.setOnClickListener(view-> {
            Intent intent = new Intent(FormaDePagoActivity.this, TarjetaActivity.class);
            startActivityForResult(intent, 0);
        });

        formaDePagoEfectivo.setOnClickListener(view-> {
            startActivity(new Intent(FormaDePagoActivity.this, agradecimientoActivity.class));
            finish();
        });
    }

    private void LimpiarVariables(){
        ListProductosActivity.totalCompraActual = 0;
        ListProductosActivity.productosDjango = new ArrayList<ProductoDjango>();
        ListProductosActivity.idsProductosComprar = new ArrayList<Integer>();
    }
}
