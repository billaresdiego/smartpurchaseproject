package com.bit.SmartPurchase;

import android.view.ViewGroup;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;

import com.bit.SmartPurchase.domain.ProductoDjango;

public class ProductoListAdapter extends ListAdapter<ProductoDjango,ProductoViewHolder> {

    private OnItemClickListener listener;

    public ProductoListAdapter(@NonNull DiffUtil.ItemCallback<ProductoDjango> diffCallbak){
        super(diffCallbak);
    }

    @NonNull
    @Override
    public ProductoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return ProductoViewHolder.create(parent);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductoViewHolder holder, int position) {
        ProductoDjango productoActual = getItem(position);
        holder.bind(productoActual.getNombre(), productoActual.getPrecio(), productoActual.getImagen());

        CardView agregarCompra = holder.itemView.findViewById(R.id.card_view_producto);
        agregarCompra.setOnClickListener(view -> {
            if(listener!=null){
                listener.onItemAdd(productoActual);
            }
        });
    }

    static class ProductoDiff extends DiffUtil.ItemCallback<ProductoDjango>{
        @Override
        public boolean areItemsTheSame(@NonNull ProductoDjango oldItem, @NonNull ProductoDjango newItem) {
            return oldItem.getId() == newItem.getId();
        }

        @Override
        public boolean areContentsTheSame(@NonNull ProductoDjango oldItem, @NonNull ProductoDjango newItem) {
            return oldItem.getNombre().equals(newItem.getNombre());
        }
    }

    public interface OnItemClickListener {
        void onItemAdd(ProductoDjango producto);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }
}
