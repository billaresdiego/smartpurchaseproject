package com.bit.SmartPurchase.repositories;

import android.app.Application;
import android.widget.Toast;

import androidx.lifecycle.MutableLiveData;

import com.bit.SmartPurchase.domain.ProductoDjango;
import com.bit.SmartPurchase.rest.RestApiClient;
import com.bit.SmartPurchase.rest.RestApiInterface;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductoRestRepository { 
    private RestApiInterface apiService = RestApiClient.getClient().create(RestApiInterface.class);

    private MutableLiveData<List<ProductoDjango>> productos = new MutableLiveData<>();
    private Application application;

    public ProductoRestRepository(Application application){
        this.application = application;
        loadProductos();
    }

    private void loadProductos(){
        productos.setValue(new ArrayList<>());
        Call<List<ProductoDjango>> call = apiService.getProductosDjango();
        call.enqueue(new Callback<List<ProductoDjango>>() {
            @Override
            public void onResponse(Call<List<ProductoDjango>> call, Response<List<ProductoDjango>> response) {
                List<ProductoDjango> misproductos = response.body();
                if(misproductos != null){
                    productos.setValue(misproductos);
                }
            }

            @Override
            public void onFailure(Call<List<ProductoDjango>> call, Throwable t) {
                Toast.makeText(application.getApplicationContext(), "No se ha podido obtener los productos: "+t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    public MutableLiveData<List<ProductoDjango>> getProductos(){
        return productos;
    }

    public void insert(ProductoDjango producto){
        apiService.addProductoDjango(producto.getNombre(), producto.getPrecio(),
                                producto.getMarca(),
                                producto.getImagen()).enqueue(new Callback<ProductoDjango>() {
            @Override
            public void onResponse(Call<ProductoDjango> call, Response<ProductoDjango> response) {
                loadProductos();
            }

            @Override
            public void onFailure(Call<ProductoDjango> call, Throwable t) {
                Toast.makeText(application.getApplicationContext(), "No se ha podido agregar el producto: "+t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
}
