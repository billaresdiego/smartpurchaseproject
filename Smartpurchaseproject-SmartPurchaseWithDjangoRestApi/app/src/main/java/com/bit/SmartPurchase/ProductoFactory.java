package com.bit.SmartPurchase;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.bit.SmartPurchase.models.ProductoViewModel;

public class ProductoFactory extends ViewModelProvider.NewInstanceFactory {

    @NonNull
    private final Application application;

    public ProductoFactory (@NonNull Application application){
        this.application = application;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if(modelClass == ProductoViewModel.class){
            return (T) new ProductoViewModel(application);
        }
        return null;
    }
}

