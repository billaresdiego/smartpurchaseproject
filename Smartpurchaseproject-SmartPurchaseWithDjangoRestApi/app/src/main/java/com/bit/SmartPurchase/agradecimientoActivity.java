package com.bit.SmartPurchase;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.bit.SmartPurchase.domain.ProductoDjango;
import com.bit.SmartPurchase.repositories.CompraRestRepository;

import java.util.ArrayList;

public class agradecimientoActivity extends AppCompatActivity {
    Button btnFinalizarCompra;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agradecimiento);
        btnFinalizarCompra = findViewById(R.id.btnFinalizarCompra);
        btnFinalizarCompra.setOnClickListener(view-> {
            CompraRestRepository compra = new CompraRestRepository(this.getApplication());
            compra.insertCompra(MainActivity.idUser, ListProductosActivity.idsProductosComprar, ListProductosActivity.totalCompraActual);
            LimpiarVariables();
            startActivity(new Intent(agradecimientoActivity.this, ListProductosActivity.class));
            finish();
        });
    }
    private void LimpiarVariables(){
        ListProductosActivity.totalCompraActual = 0;
        ListProductosActivity.productosDjango = new ArrayList<ProductoDjango>();
        ListProductosActivity.idsProductosComprar = new ArrayList<Integer>();
    }

}