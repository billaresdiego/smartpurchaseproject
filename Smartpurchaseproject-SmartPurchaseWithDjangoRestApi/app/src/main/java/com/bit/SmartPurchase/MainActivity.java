package com.bit.SmartPurchase;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.Toast;

import com.bit.SmartPurchase.domain.UsuarioDjango;
import com.bit.SmartPurchase.rest.RestApiClient;
import com.bit.SmartPurchase.rest.UserService;
import com.google.android.material.textfield.TextInputEditText;

import androidx.appcompat.app.AppCompatActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    TextInputEditText username, password;
    Button btnLogin, btnIrRegistro;
    public static int idUser = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        username = findViewById(R.id.edUsername);
        password = findViewById(R.id.edPassword);
        btnLogin = findViewById(R.id.btnLogin);
        btnIrRegistro = findViewById(R.id.btnIrRegistro);

        btnLogin.setOnClickListener(view-> {
            onClickAction("login");
        });

        btnIrRegistro.setOnClickListener(view -> {
            onClickAction("registro");
        });
    }

    private void onClickAction(String action){
        if (action == "registro") {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    startActivity(new Intent(MainActivity.this, RegistroActivity.class));
                }
            }, 700);
        } else {
            if (TextUtils.isEmpty(username.getText().toString()) || TextUtils.isEmpty(password.getText().toString())) {
                Toast.makeText(MainActivity.this, "Username / Password Required", Toast.LENGTH_LONG).show();
            } else {
                login2();
            }
        }
    }

    private void login2() {
        Call<UsuarioDjango> loginResponseCall = RestApiClient.getClient().create(UserService.class).inciarSesion(
                username.getText().toString(), password.getText().toString());
        loginResponseCall.enqueue(new Callback<UsuarioDjango>() {
            @Override
            public void onResponse(Call<UsuarioDjango> call, Response<UsuarioDjango> response) {
                int loginResponse = response.code();
                if (loginResponse == 200) {
                    Toast.makeText(MainActivity.this, "Login Successful", Toast.LENGTH_LONG).show();
                    idUser = response.body().getId();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            finish();
                            startActivity(new Intent(MainActivity.this, ListProductosActivity.class));
                        }
                    }, 700);
                } else {
                    Toast.makeText(MainActivity.this, "Login Failed", Toast.LENGTH_LONG).show();

                }

            }


            @Override
            public void onFailure(Call<UsuarioDjango> call, Throwable t) {
                Toast.makeText(MainActivity.this, "Throwable " + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();

            }
        });
    }
}