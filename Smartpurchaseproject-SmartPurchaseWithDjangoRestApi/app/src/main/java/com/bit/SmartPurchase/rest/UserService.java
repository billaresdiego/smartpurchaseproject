package com.bit.SmartPurchase.rest;

import com.bit.SmartPurchase.domain.UsuarioDjango;


import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface UserService {
    @FormUrlEncoded
    @POST("/api/login/")
    Call<UsuarioDjango> inciarSesion(@Field("username") String username, @Field("password") String password);
}
