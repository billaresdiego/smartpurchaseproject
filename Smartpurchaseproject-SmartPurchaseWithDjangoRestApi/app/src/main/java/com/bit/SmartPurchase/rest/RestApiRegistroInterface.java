package com.bit.SmartPurchase.rest;

import com.bit.SmartPurchase.JsonResults.User;
import com.bit.SmartPurchase.domain.UsuarioDjango;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.POST;


public interface RestApiRegistroInterface {

    @FormUrlEncoded
    @POST("/api/register/")
    Call<UsuarioDjango> registroUsuarioDjango(@Field("username") String username,
                                              @Field("email") String email,
                                              @Field("password") String password);
    @POST("/api/register/")
    Call<User> registroUsuarioJson(@Body User nuevoUsuario);
}
