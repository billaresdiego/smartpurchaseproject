package com.bit.SmartPurchase;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.bit.SmartPurchase.JsonResults.User;
import com.bit.SmartPurchase.rest.RestApiClient;
import com.bit.SmartPurchase.rest.RestApiRegistroInterface;
import com.google.android.material.textfield.TextInputEditText;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistroActivity extends AppCompatActivity {
    Button btnRegistro;
    TextInputEditText contrasena, userName, email;
    RadioButton hombre, mujer, otros;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        email = findViewById(R.id.edEmail);
        contrasena = findViewById(R.id.edContrasena);
        btnRegistro = findViewById(R.id.btnRegistro);
        userName = findViewById(R.id.userName);
        hombre = (RadioButton) findViewById(R.id.radioHombre);
        mujer = (RadioButton) findViewById(R.id.radioMujer);
        otros = (RadioButton) findViewById(R.id.radioOtros);



        btnRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (TextUtils.isEmpty(userName.getText().toString())
                || TextUtils.isEmpty(email.getText().toString()) || TextUtils.isEmpty(contrasena.getText().toString())) {
                    Toast.makeText(RegistroActivity.this, "Se requiere llenar todos los campos", Toast.LENGTH_LONG).show();
                } else {
                    registrado();
                }

            }
        });
    }

    private void registrado() {
        User registro = new User(userName.getText().toString(),
                                    email.getText().toString(),
                                    contrasena.getText().toString(),
                                    opcionSexo());
        Call<User> registroResponseCall = RestApiClient.getClient().create(RestApiRegistroInterface.class).registroUsuarioJson(
                                                                            registro);
        System.out.println(registroResponseCall.request());
        registroResponseCall.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                int loginResponse = response.code();
                System.out.println(loginResponse);
                if (loginResponse == 200) {
                    Toast.makeText(RegistroActivity.this, "Se ha registrado satisfactoriamente", Toast.LENGTH_LONG).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            finish();
                            startActivity(new Intent(RegistroActivity.this, MainActivity.class));
                        }
                    }, 700);
                } else {
                    Toast.makeText(RegistroActivity.this, "El registro ha fallado", Toast.LENGTH_LONG).show();

                }

            }
            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Toast.makeText(RegistroActivity.this, "Desechable " + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();

            }
        });
    }

    private String opcionSexo() {
        String seleccion = "";

        if (hombre.isChecked() == true) {
            seleccion = "hombre";
        }

        if (mujer.isChecked() == true) {
            seleccion = "mujer";
        }

        if (otros.isChecked() == true) {
            seleccion = "otros";
        }

        return seleccion;
    }

}
