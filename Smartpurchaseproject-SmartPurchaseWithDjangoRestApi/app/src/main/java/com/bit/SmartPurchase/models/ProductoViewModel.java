package com.bit.SmartPurchase.models;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.bit.SmartPurchase.domain.ProductoDjango;
import com.bit.SmartPurchase.repositories.ProductoRestRepository;

import java.util.List;

public class ProductoViewModel extends AndroidViewModel {

    private ProductoRestRepository productoRepository;
    private final LiveData<List<ProductoDjango>> productos;

    public ProductoViewModel(Application application){
        super(application);
        productoRepository = new ProductoRestRepository(application);
        productos = productoRepository.getProductos();
    }

    public LiveData<List<ProductoDjango>> getProductos(){
        return productos;
    }

    public void insert(ProductoDjango producto){
        productoRepository.insert(producto);
    }

}
