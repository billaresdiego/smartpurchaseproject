package com.bit.SmartPurchase.repositories;

import android.app.Application;
import android.widget.Toast;

import androidx.lifecycle.MutableLiveData;

import com.bit.SmartPurchase.domain.CompraDjango;
import com.bit.SmartPurchase.rest.RestApiClient;
import com.bit.SmartPurchase.rest.RestApiCompraInterface;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CompraRestRepository {
    private RestApiCompraInterface apiService = RestApiClient.getClient().create(RestApiCompraInterface.class);

    private MutableLiveData<List<CompraDjango>> compras = new MutableLiveData<>();
    private Application application;

    public CompraRestRepository(Application application){
        this.application = application;
    }

    public MutableLiveData<List<CompraDjango>> getCompras(){
        return compras;
    }

    public void insertCompra(int usuarioId, ArrayList<Integer> productosIds, int totalCompra){
        CompraDjango compraDjango = new CompraDjango(usuarioId, productosIds, totalCompra);
        Call<CompraDjango> compra = apiService.addCompraDjango(compraDjango);
        System.out.println(compra.request());
        compra.enqueue(new Callback<CompraDjango>() {
            @Override
            public void onResponse(Call<CompraDjango> call, Response<CompraDjango> response) {

            }

            @Override
            public void onFailure(Call<CompraDjango> call, Throwable t) {
                Toast.makeText(application.getApplicationContext(), "No se ha podido agregar la compra: "+t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
}


