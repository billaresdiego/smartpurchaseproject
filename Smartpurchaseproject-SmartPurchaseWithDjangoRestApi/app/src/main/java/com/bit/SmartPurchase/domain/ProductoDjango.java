package com.bit.SmartPurchase.domain;

import com.google.gson.annotations.SerializedName;

public class ProductoDjango {
    @SerializedName("pk")
    private int id;
    @SerializedName("nombre")
    private String nombre;
    @SerializedName("cantidad")
    private int cantidad;
    @SerializedName("precio")
    private int precio;
    @SerializedName("marca")
    private String marca;
    @SerializedName("imagen_url")
    private String imagen;

    public int getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public int getCantidad() { return cantidad; }

    public int getPrecio() {
        return precio;
    }

    public String getMarca() {
        return marca;
    }

    public String getImagen() {
        return imagen;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setCantidad(int cantidad) { this.cantidad = cantidad; }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public ProductoDjango() {
    }

    public ProductoDjango(int id, String nombre, int precio, String marca, String imagen) {
        this.id = id;
        this.nombre = nombre;
        this.precio = precio;
        this.marca = marca;
        this.imagen = imagen;
    }
}
