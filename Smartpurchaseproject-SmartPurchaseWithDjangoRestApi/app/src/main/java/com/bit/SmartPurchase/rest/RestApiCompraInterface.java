package com.bit.SmartPurchase.rest;

import com.bit.SmartPurchase.domain.CompraDjango;
import com.bit.SmartPurchase.domain.ProductoDjango;
import com.bit.SmartPurchase.domain.UsuarioDjango;


import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface RestApiCompraInterface {

    @POST("/api/compra/")
    Call<CompraDjango> addCompraDjango(@Body CompraDjango compra);
}
