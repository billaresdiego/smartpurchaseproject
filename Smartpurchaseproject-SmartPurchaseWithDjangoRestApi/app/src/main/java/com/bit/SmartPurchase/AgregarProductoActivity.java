package com.bit.SmartPurchase;

import com.bit.SmartPurchase.domain.ProductoDjango;
import com.google.android.material.appbar.MaterialToolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class AgregarProductoActivity<onClick> extends OptionsMenuActivity {
    public static final String EXTRA_MSG_ID = "com.bit.SmartPurchase.MSG_GUARDAR_ID";
    public static final String EXTRA_MSG_NOMBRE = "com.bit.SmartPurchase.MSG_GUARDAR_NOMBRE";
    public static final String EXTRA_MSG_PRECIO = "com.bit.SmartPurchase.MSG_GUARDAR_PRECIO";
    public static final String EXTRA_MSG_MARCA = "com.bit.SmartPurchase.MSG_GUARDAR_MARCA";
    public static final String EXTRA_MSG_IMAGEN = "com.bit.SmartPurchase.MSG_GUARDAR_IMAGEN";


    private TableRow filaProducto;
    private TableLayout tablaProductos;
    private TextView totalCompra;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_compra);

        tablaProductos = findViewById(R.id.tableLayout);
        totalCompra = findViewById(R.id.totalCompra);
        ArmarListaProductos();


        final Button btnElegirFormaDePago = findViewById(R.id.btnElegirFormaDePago);
        btnElegirFormaDePago.setOnClickListener(view -> {
            startActivity(new Intent(AgregarProductoActivity.this, FormaDePagoActivity.class));
        });

        /*final Button btnIngresarMasProductos = findViewById(R.id.btnIngresarMasProductos);
        btnIngresarMasProductos.setOnClickListener(view ->{
            startActivity(new Intent(AgregarProductoActivity.this, ListProductosActivity.class));
        })*/
        final Button btnIngresarMasProductos = findViewById(R.id.btnIngresarMasProductos);
        btnIngresarMasProductos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AgregarProductoActivity.super.onBackPressed();
            }
        });

        MaterialToolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setSubtitle(R.string.app_subtitle);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AgregarProductoActivity.super.onBackPressed();
            }
        });

    }
    private void ArmarListaProductos(){
        int totalCompraProductos = 0;
        for (int i = 0; i < ListProductosActivity.productosDjango.size(); i++) {
            ProductoDjango producto = ListProductosActivity.productosDjango.get(i);
            filaProducto = new TableRow(this.getApplicationContext());
            TextView insertarFila = crearTextViewParaFila(producto.getNombre());
            filaProducto.addView(insertarFila);
            insertarFila = crearTextViewParaFila("  " + producto.getCantidad());
            filaProducto.addView(insertarFila);
            insertarFila = crearTextViewParaFila(String.valueOf(producto.getPrecio()));
            totalCompraProductos += producto.getPrecio();
            filaProducto.addView(insertarFila);
            tablaProductos.addView(filaProducto);
        }
        totalCompra.setText("Total: " + totalCompraProductos);
    }

    private TextView crearTextViewParaFila(String texto) {
        TextView textView = new TextView(this.getApplicationContext());
        CharSequence charSequence = texto;
        textView.setText(texto);
        return textView;
    }
}