package com.bit.SmartPurchase.domain;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CompraDjango {
    @SerializedName("pk")
    private int compra_id;
    @SerializedName("idUsuario")
    private int usuario_id;
    @SerializedName("productos")
    private ArrayList<Integer> productos;
    @SerializedName("monto_maximo")
    private int montoMaximo;

    public CompraDjango(int compra_id, int usuario_id, ArrayList<Integer> productos, int montoMaximo) {
        this.compra_id = compra_id;
        this.usuario_id = usuario_id;
        this.productos = productos;
        this.montoMaximo = montoMaximo;
    }

    public CompraDjango(int usuario_id, ArrayList<Integer> productos, int montoMaximo) {
        this.usuario_id = usuario_id;
        this.productos = productos;
        this.montoMaximo = montoMaximo;
    }

    public int getUsuario_id() {
        return usuario_id;
    }

    public void setUsuario_id(int usuario_id) {
        this.usuario_id = usuario_id;
    }

    public ArrayList<Integer> getProductos() {
        return productos;
    }

    public void setProductos(ArrayList<Integer> productos) {
        this.productos = productos;
    }

    public int getCompra_id() {
        return compra_id;
    }

    public void setCompra_id(int compra_id) {
        this.compra_id = compra_id;
    }

    @Override
    public String toString() {
        return "Compra{" +
                '"' + "idUsuario" + '"' + ":" + " " +  '"' + String.valueOf(this.usuario_id) + '"' +
                ", " + '"' + "productos" + '"' + ":"  + " " + '"' + String.valueOf(this.productos) + '"' +
                ", " + '"' + "monto_maximo" + '"' + ":"  + " " + '"'   + String.valueOf(this.montoMaximo) + '"' +
                '}';
    }
}
