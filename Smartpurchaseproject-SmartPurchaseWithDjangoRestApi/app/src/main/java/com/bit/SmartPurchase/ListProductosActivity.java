package com.bit.SmartPurchase;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.bit.SmartPurchase.domain.ProductoDjango;
import com.bit.SmartPurchase.repositories.CompraRestRepository;
import com.google.android.material.appbar.MaterialToolbar;

import com.bit.SmartPurchase.models.ProductoViewModel;
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ListProductosActivity extends OptionsMenuActivity implements NavigationView.OnNavigationItemSelectedListener{

    private ProductoViewModel productoViewModel;
    public static final int NEW_PRODUCTO_REQ_CODE = 1;
    public static final int UPDATE_PRODUCTO_REQ_CODE = 2;
    public static ArrayList<Integer> idsProductosComprar;
    public static List<ProductoDjango> productosDjango;
    public static int totalCompraActual;
    private EditText textIngresarMonto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_productos);
        final ProductoListAdapter adapter = new ProductoListAdapter(new ProductoListAdapter.ProductoDiff());
        IncializarVariables(adapter);
        FloatingActionButton fab = findViewById(R.id.btnAgregar);

        fab.setOnClickListener( view -> {
            CambiarAtributosEventoBotonAgregar();

            Intent intent = new Intent(ListProductosActivity.this, AgregarProductoActivity.class);
            startActivityForResult(intent, 1);
        });

        adapter.setOnItemClickListener(new ProductoListAdapter.OnItemClickListener() {
            @Override
            public void onItemAdd(ProductoDjango producto) {

                AlertDialog.Builder builder = new AlertDialog.Builder(ListProductosActivity.this);
                builder.setMessage(R.string.titulo_agregar_compra);
                builder.setTitle(R.string.titulo_agregar_compra);

                builder.setPositiveButton(R.string.si, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        EstablecerCompraEventoClickAdaptador(producto);
                    }
                });
                builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(getApplicationContext(), R.string.no_agregado_compra, Toast.LENGTH_LONG).show();
                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();

            }


        });

        MaterialToolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setSubtitle(R.string.app_subtitle);

        DrawerLayout drawerLayout = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode==NEW_PRODUCTO_REQ_CODE && resultCode == RESULT_OK){
            ProductoDjango producto = new ProductoDjango();
            producto.setNombre(data.getStringExtra(AgregarProductoActivity.EXTRA_MSG_NOMBRE));
            producto.setPrecio(Integer.valueOf(data.getStringExtra(AgregarProductoActivity.EXTRA_MSG_PRECIO)));
            producto.setMarca(data.getStringExtra(AgregarProductoActivity.EXTRA_MSG_MARCA));
            producto.setImagen(data.getStringExtra(AgregarProductoActivity.EXTRA_MSG_IMAGEN));
            Logger.getAnonymousLogger().log(Level.SEVERE, producto.getNombre()+" "+producto.getImagen());
            productoViewModel.insert(producto);

        }
        else {
            Toast.makeText(getApplicationContext(), R.string.no_guardado, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.nav_favoritos:
                Toast.makeText(getApplicationContext(), R.string.menu_favoritos, Toast.LENGTH_LONG).show();
                break;
            case R.id.nav_perfil:
                Toast.makeText(getApplicationContext(), R.string.menu_perfil, Toast.LENGTH_LONG).show();
                break;
            case R.id.nav_send:
                Toast.makeText(getApplicationContext(), R.string.menu_send, Toast.LENGTH_LONG).show();
                break;
            default:
                throw new IllegalArgumentException("Opcion no existente");
        }

        return true;
    }

    private void IncializarVariables(ProductoListAdapter adapter){
        idsProductosComprar = new ArrayList<Integer>();
        totalCompraActual = 0;
        productosDjango = new ArrayList<ProductoDjango>();
        textIngresarMonto = findViewById(R.id.ingresarMonto);
        RecyclerView recyclerView = findViewById(R.id.recyclerViewProductos);
        recyclerView.setAdapter(adapter);
        int numberOfColumns = 2;
        recyclerView.setLayoutManager(new GridLayoutManager(this, numberOfColumns));
        productoViewModel = new ViewModelProvider(this, new ProductoFactory(getApplication())).get(ProductoViewModel.class);
        productoViewModel.getProductos().observe(this, productos -> {
            adapter.submitList(productos);
        });
    }

    private void ProductoSeleccionadoCarrito(ProductoDjango producto){
        if(productosDjango.contains(producto)){
            int posicionProducto = productosDjango.indexOf(producto);
            int cantidadProducto = productosDjango.get(posicionProducto).getCantidad() + 1;
            int precioTotal = productosDjango.get(posicionProducto).getPrecio() * cantidadProducto;
            productosDjango.get(posicionProducto).setCantidad(cantidadProducto);
            productosDjango.get(posicionProducto).setPrecio(precioTotal);
        }else{
            producto.setCantidad(1);
            productosDjango.add(producto);
        }
    }
    private void CambiarAtributosEventoBotonAgregar(){
        textIngresarMonto.getText().clear();
    }
    private void EstablecerCompraEventoClickAdaptador(ProductoDjango producto){
        int totalCompraConProductoActual = producto.getPrecio() + totalCompraActual;
        if(textIngresarMonto.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(), "Ingresar un monto para comprar", Toast.LENGTH_LONG).show();
        }
        else {
            if (Integer.parseInt(textIngresarMonto.getText().toString()) >= totalCompraConProductoActual) {
                totalCompraConProductoActual = 0;
                totalCompraActual += producto.getPrecio();
                idsProductosComprar.add(producto.getId());
                ProductoSeleccionadoCarrito(producto);
                Toast.makeText(getApplicationContext(), R.string.agregado_compra, Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), R.string.exceso_monto, Toast.LENGTH_LONG).show();
            }
        }
    }
}