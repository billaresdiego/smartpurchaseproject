package com.bit.SmartPurchase;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

public class ProductoViewHolder  extends RecyclerView.ViewHolder {
    private final TextView nombreItemView;
    private final TextView precioItemView;
    private final ImageView imagenView;

    private ProductoViewHolder(View itemView){
        super(itemView);
        nombreItemView = itemView.findViewById(R.id.textViewNombre);
        precioItemView = itemView.findViewById(R.id.textViewPrecio);
        imagenView = itemView.findViewById(R.id.imagen);
    }

    public void bind(String nombre, double precio, String url){
        nombreItemView.setText(nombre);
        precioItemView.setText(convertirAString(precio));
        Picasso.with(itemView.getContext())
                .load(url)
                .centerCrop()
                .fit()
                .into(imagenView);
    }

    private String convertirAString(double precioProducto){
        return Double.toString(precioProducto);
    }

    static ProductoViewHolder create(ViewGroup parent){
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.producto_item, parent, false);
        return new ProductoViewHolder(view);
    }
}
