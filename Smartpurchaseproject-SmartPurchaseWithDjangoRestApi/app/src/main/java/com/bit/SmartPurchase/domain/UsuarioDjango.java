package com.bit.SmartPurchase.domain;

import com.google.gson.annotations.SerializedName;

public class UsuarioDjango {
    @SerializedName("user_id")
    private int id;
    @SerializedName("usuario")
    private String usuario;
    @SerializedName("email")
    private String email;
    @SerializedName("contrasena")
    private String contrasena;

    public UsuarioDjango(int id, String usuario, String email, String contrasena) {
        this.id = id;
        this.usuario = usuario;
        this.email = email;
        this.contrasena = contrasena;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }
}
