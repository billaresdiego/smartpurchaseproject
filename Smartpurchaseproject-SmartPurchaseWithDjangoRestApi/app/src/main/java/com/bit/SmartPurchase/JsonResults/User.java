package com.bit.SmartPurchase.JsonResults;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User {

    @SerializedName("username")
    @Expose
    private String usuario;

    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("password")
    @Expose
    private String contrasena;

    @SerializedName("sexo")
    @Expose
    private String sexo;

    public User(String usuario, String email, String contrasena, String sexo) {
        this.usuario = usuario;
        this.email = email;
        this.contrasena = contrasena;
        this.sexo = sexo;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }


    @Override
    public String toString() {
        return "User{" +
                '"' + "username" + '"' + ":" + " " +  '"' + String.valueOf(this.usuario) + '"' +
                ", " + '"' + "email" + '"' + ":"  + " " + '"' + String.valueOf(this.email) + '"' +
                ", " + '"' + "password" + '"' + ":"  + " " + '"'   + String.valueOf(this.contrasena) + '"' +
                ", " + '"' + "sexo" + '"' + ":"  + " " + '"'   + String.valueOf(this.sexo) + '"' +
                '}';
    }
}
